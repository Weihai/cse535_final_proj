# CSE535 mobile computing final project

In this project, we will develop a continuous meal detection method from glucose monitor data of a subject. You will be given CGM sensor outputs for a given person for nearly 6 months. The CGM output is every 5 mins. You will also be given ground truth for every meal instance. You will have to develop a continuous meal detection algorithm.
- Algorithm development, SARIMA
- RNN based meal detection

Description:  https://asu.instructure.com/courses/46637/files/13456515?module_item_id=2934445

Q&A: https://asu.zoom.us/rec/play/tJwsduGqpjs3GNSTtASDBqd8W9S0K_-sgCQW_fcKmhu1WyNRYVrwYucbMes56WrUiF7VQmZfyhtByUEU?continueMode=true&_x_zm_rtaid=WOiezUnRTxeJkbwxdabAVg.1586299564391.2e4318d9bc7db7da8fe2de8da9a78c2b&_x_zm_rhtaid=271

Proj sign up: https://docs.google.com/spreadsheets/d/1FHUGLoweJGGMbybFzdVGo4mAWaq8kDnQem5gBT6Ly9g/edit?usp=sharing


## final project requirement
1. Source Code
2. Report 6 pages max
3. Video demo of the project tasks max 15 mins